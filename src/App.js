import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ProductProvider } from "./context/ProductContext";
import ChildrenPage from "./pages/ChildrenPage";
import CreateProductPage from "./pages/CreateProductPage";
import FormPage from "./pages/FormPage";
import LandingPage from "./pages/LandingPage";
import ListProductPage from "./pages/ListProductPage";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import TableProductPage from "./pages/TableProductPage";
import UIPage from "./pages/UIPage";
import UpdateProductPage from "./pages/UpdateProductPage";
import ProtectedRoute from "./wrapper/ProtectedRoute";
import GuestRoute from "./wrapper/GuestRoute";
import './App.css';

function App() {
  return (
    <div>
      <BrowserRouter>
        <ProductProvider>
          <Routes>
            <Route path="/" element={<ListProductPage />} />
            <Route path="/update/:id" element={<UpdateProductPage />} />
            <Route path="/children" element={<ChildrenPage />} />
            <Route path="/landing" element={<LandingPage />} />
            <Route path="/ui-kit" element={<UIPage />} />

            <Route element={<ProtectedRoute />}>
              <Route path="/table" element={<TableProductPage />} />
              <Route path="/form" element={<FormPage />} />
              <Route path="/create" element={<CreateProductPage />} />
            </Route>

            <Route element={<GuestRoute />}>
              <Route path="/login" element={<LoginPage />} />
              <Route path="/register" element={<RegisterPage />} />
            </Route>
          </Routes>
        </ProductProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
