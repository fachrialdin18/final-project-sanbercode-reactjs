import React, { useContext, useEffect } from "react";
import { Helmet } from "react-helmet";
import Product from "../components/Product";
import Layout from "../components/Layout";
import { ProductContext } from "../context/ProductContext";
import BanerCarousel1 from '../images/ps2.webp'

function ListProductPage() {
  const { products, fetchProducts, loading, status } =
    useContext(ProductContext);

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <section>
      <Helmet>
        <title>List Product Page</title>
      </Helmet>
      <Layout>
      
        
      <div id="default-carousel" className="relative w-full" data-carousel="slide">
          <div className="relative h-56 overflow-hidden md:h-screen">
              <div className="ease-in-out shadow-lg" data-carousel-item>
                  <img src={BanerCarousel1} className="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="..." />
              </div>
          </div>
      </div>


        <h1 className="my-8 text-3xl font-semibold text-center text-gray-700">List Product</h1>
        {loading === false ? (
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4 mx-5 center">
            {products.map((item, index) => {
              if (localStorage.getItem("username") === item.user.username) {
              return <Product data={item} />;
            }
            })}
          </div>
        ) : (
          <h1 className="text-center my-6 text-2xl text-gray-500 font-semibold">Loading, Please Wait...</h1>
        )}
        {loading === false ? (
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4 mx-5 center">
            {products.map((item, index) => {
              if (localStorage.getItem("username") === null) {
              return <Product data={item} />;
            }
            })}
          </div>
        ) : (
          <h1 className="text-center my-6 text-2xl text-gray-500 font-semibold"></h1>
        )}
      </Layout>
    </section>
  );
}

export default ListProductPage;