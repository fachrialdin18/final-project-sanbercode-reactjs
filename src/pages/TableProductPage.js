import React, { useContext, useEffect } from "react";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";
import TableProduct from "../components/TableProduct";
import { ProductContext } from "../context/ProductContext";

function TableProductPage() {
  const { fetchProducts } = useContext(ProductContext);
  useEffect(() => {
    fetchProducts();
  }, []);
  return (
    <div>
      <Helmet>
        <title>Table Product Page</title>
      </Helmet>
      <Layout>
        <TableProduct />
      </Layout>
    </div>
  );
}

export default TableProductPage;
