import React from "react";
import { Helmet } from "react-helmet";
import Form from "../components/Form";
import Layout from "../components/Layout";

function CreateProductPage() {
  return (
    <div>
      <Helmet>
        <title>Form Create Product Page</title>
      </Helmet>
      <Layout>
        <Form />
      </Layout>
    </div>
  );
}

export default CreateProductPage;

