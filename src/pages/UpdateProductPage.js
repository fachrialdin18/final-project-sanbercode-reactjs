import React from "react";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";
import UpdateForm from "../components/UpdateForm";

function UpdateProductPage() {
  return (
    <div>
      <Helmet>
        <title>Update Product Page</title>
      </Helmet>
      <Layout>
        <UpdateForm />
      </Layout>
    </div>
  );
}

export default UpdateProductPage;
