import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Navbar from '../components/Navbar';

function ProfilePage() {

  const [data, setData] = useState({});

  const fetchProfile = async() => {
    try {
        const response = await axios.get("https://api-project.amandemy.co.id/api/profile",
        {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },    
        }
    );
        setData(response.data.data);
    } 
    catch (error) {
        alert("Terjadi suatu error");
    }
  };

  useEffect = (() => {
    fetchProfile();
  }, []);

  return (
    <div className='mt-6'>
        <Navbar />
        
        <h1>Hallo, {data.username}</h1>

      
    </div>
  )
}

export default ProfilePage
