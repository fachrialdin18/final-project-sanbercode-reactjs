import axios from "axios";
import { useFormik } from "formik";
import React from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";

const rulesSchema = Yup.object({
  name: Yup.string().required("Nama Pengguna wajib diisi"),
  username: Yup.string().required("Username Pengguna wajib diisi"),
  email: Yup.string()
    .required("Email Pengguna wajib diisi")
    .email("Email tidak valid"),
  password: Yup.string().required("Password wajib diisi"),
  password_confirmation: Yup.string().required(
    "Password Konfirmasi wajib diisi"
  ),
});

function RegisterForm() {
  const navigate = useNavigate();

  const initialState = {
    name: "",
    username: "",
    email: "",
    password: "",
    password_confirmation: "",
  };

  const onRegister = async (values) => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/register",
        {
          name: values.name,
          username: values.username,
          email: values.email,
          password: values.password,
          password_confirmation: values.password_confirmation,
        }
      );
      alert("Berhasil Melakukan Registrasi !");
      resetForm();
      // navigasi ke halaman table
      navigate("/login");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
    setFieldValue,
    setFieldTouched,
  } = useFormik({
    initialValues: initialState,
    onSubmit: onRegister,
    validationSchema: rulesSchema,
  });

  return (
    <div className="my-5 mx-5">
      <h1 className="mt-3 text-center text-2xl text-gray-700 font-semibold">Selamat Datang, Silahkan Register !</h1>
        <div className="mt-2 rounded-lg py-2 max-w-xl sm: md:max-w-2xl md:mx-auto mx-10">
          <form className="bg-white shadow-md rounded px-5 pt-6 pb-8 mb-4 w-full">
            <div className="mb-4">
              <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="Nama">
                Nama
              </label>

              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                placeholder="Masukkan Nama"
                onChange={handleChange}
                onBlur={handleBlur}
                name="name"
                value={values.name}
              />

              <p className="text-red-600">
                    {touched.name === true && errors.name}
              </p>
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="Username">
                Username
              </label>

              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                placeholder="Masukkan Username"
                onChange={handleChange}
                onBlur={handleBlur}
                name="username"
                value={values.username}
              />

              <p className="text-red-600">
                  {touched.username === true && errors.username}
              </p>
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="Email">
                Email
              </label>

              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                placeholder="Masukkan Email"
                onChange={handleChange}
                onBlur={handleBlur}
                name="email"
                value={values.email}
              />

              <p className="text-red-600">
                    {touched.email === true && errors.email}
              </p>
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                Kata Sandi
              </label>

              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="password"
                placeholder="Masukkan Kata Sandi"
                onChange={handleChange}
                onBlur={handleBlur}
                name="password"
                value={values.password}
              />

              <p className="text-red-600">
                  {touched.password === true && errors.password}
              </p>
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="confirmation_password">
                Konfirmasi Kata Sandi
              </label>

              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="password"
                placeholder="Masukkan Konfirmasi Kata Sandi"
                onChange={handleChange}
                onBlur={handleBlur}
                name="password_confirmation"
                value={values.password_confirmation}
              />

              <p className="text-red-600">
                  {touched.password_confirmation === true && errors.password_confirmation}
              </p>
            </div>
            <div className="flex items-center justify-between">
              <button
                className="w-full text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
                type="button"
                onClick={handleSubmit}
              >
                Buat Akun
              </button>
            </div>
          </form>
        </div>
    </div>
  );
}

export default RegisterForm;