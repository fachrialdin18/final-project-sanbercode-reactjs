import axios from "axios";
import React, { useContext, useState } from "react";
import { useFormik } from "formik"; 
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { ProductContext } from "../context/ProductContext";

const rulesSchema = Yup.object({
  name: Yup.string().required("Nama Wajib Diisi !"),
  stock: Yup.number().required("Stock Wajib Diisi !"),
  harga: Yup.number().required("Harga Wajib Diisi !"),
  harga_diskon: Yup.number().required("Harga Diskon Wajib Diisi !"),
  category: Yup.string().required("Kategori Wajib Diisi !"),
  image_url: Yup.string()
    .required("Link Gambar Wajib Diisi !")
    .url("Link Gambar tidak valid"),
});

function Form() {
  const { fetchProducts } = useContext(ProductContext);
  const initialState = {
    name: "",
    stock: "",
    harga: "",
    is_diskon: false,
    harga_diskon: "",
    category: "",
    image_url: "",
    description: "",
  };

  const navigate = useNavigate();

  const storeArticle = async (values) => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/final/products",
        {
          name: values.name,
          stock: values.stock,
          harga: values.harga,
          is_diskon: values.is_diskon,
          harga_diskon: values.harga_diskon,
          category: values.category,
          image_url: values.image_url,
          description: values.description,
        },
        {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        }
      );
      alert("Berhasil Membuat Data !");
      // memannggil data kembali
      fetchProducts();
      resetForm();
      // navigasi ke halaman table
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
  } = useFormik({
      initialValues: initialState,
      onSubmit: storeArticle,
      validationSchema: rulesSchema,
  });

  return (
    <div class="p-6 max-w-3xl mx-auto bg-white rounded-xl shadow-md flex flex-col space-y-6 mt-5 mb-5">

      <h1 className="text-blue-700 text-xl font-semibold">Formulir Product</h1>
      <div class="border border-gray-300 border-b-3"></div>

      <div class="flex flex-col space-y-2">
        <label for="namaBarang" class="text-sm font-medium text-gray-700 mr-2">Nama Barang</label>
        <input onChange={handleChange} onBlur={handleBlur} value={values.name} type="text" placeholder="Masukkan Nama Barang" id="name" name="name" class="border rounded px-4 py-2 focus:outline-none focus:ring-2 focus:ring-blue-400" required />
        <p className="text-red-600 mt-1">
            {touched.name === true && errors.name}
        </p>
      </div>
      
      <div class="flex flex-col space-y-2 md:flex-row md:space-x-2 md:space-y-0">
        <div class="flex-1">
          <label for="stock" class="text-sm font-medium text-gray-700 mr-2">Stock Barang</label>
          <input onChange={handleChange} onBlur={handleBlur} value={values.stock} type="number" placeholder="Masukkan Stock Barang" id="stock" name="stock" class="border rounded px-4 py-2 focus:outline-none focus:ring-2 focus:ring-blue-400" required />
          <p className="text-red-600 mt-1">
            {touched.stock === true && errors.stock}
          </p>
        </div>
        <div class="flex-1">
          <label for="harga" class="text-sm font-medium text-gray-700 mr-2">Harga Barang</label>
          <input onChange={handleChange} onBlur={handleBlur} value={values.harga} type="text" placeholder="Masukkan Harga Barang" id="harga" name="harga" class="border rounded px-4 py-2 focus:outline-none focus:ring-2 focus:ring-blue-400" required />
          <p className="text-red-600 mt-1">
            {touched.harga === true && errors.harga}
          </p>
        </div>
      </div>

      <div class="flex flex-col space-y-2 md:flex-row md:space-x-2 md:space-y-0">
        <div class="flex-1">
          <label for="harga_diskon" class="text-sm font-medium text-gray-700 mr-2">Harga Diskon</label>
          <input onChange={handleChange} onBlur={handleBlur} value={values.harga_diskon} type="text" placeholder="Masukkan Harga Diskon" id="harga_diskon" name="harga_diskon" class="border rounded px-4 py-2 focus:outline-none focus:ring-2 focus:ring-blue-400" required />
          <p className="text-red-600 mt-1">
            {touched.harga_diskon === true && errors.harga_diskon}
          </p>
        </div>
        <div class="flex-1">
          <label for="category" class="text-sm font-medium text-gray-700 mr-2">Kategori Barang</label>
          <select onChange={handleChange} onBlur={handleBlur} value={values.category} name="category" id="category" class="border rounded px-4 py-2 focus:outline-none focus:ring-2 focus:ring-blue-400">
          <option value="" className="bg-gray-800 text-blue-400">Silahkan Pilih Kategori</option>
            <option value="teknologi">Teknologi</option>
            <option value="makanan">Makanan</option>
            <option value="minuman">Minuman</option>
            <option value="hiburan">Hiburan</option>
            <option value="kendaraan">Kendaraan</option>
          </select>
          <p className="text-red-600 mt-1">
            {touched.category === true && errors.category}
          </p>
        </div>
      </div>

      <div class="flex flex-col space-y-2">
        <label for="image_url" class="text-sm font-medium text-gray-700 mr-2">Gambar Barang</label>
        <input onChange={handleChange} onBlur={handleBlur} value={values.image_url} type="text" placeholder="Masukkan Gambar Barang" id="image_url" name="image_url" class="border rounded px-4 py-2 focus:outline-none focus:ring-2 focus:ring-blue-500" required />
        <p className="text-red-600 mt-1">
          {touched.image_url === true && errors.image_url}
        </p>
      </div>

      <div class="flex flex-col space-y-2">
        <label for="description" class="text-sm font-medium text-gray-700 mr-2">Deskripsi Barang ( Optional )</label>
        <textarea onChange={handleChange} onBlur={handleBlur} value={values.description} id="description" placeholder="Masukkan Deskripsi Barang" name="description" rows="4" class="border rounded px-4 py-2 focus:outline-none focus:ring-2 focus:ring-blue-500"></textarea>
      </div>
      
      <button
        className="w-full bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 text-white"
        type="button"
        onClick={handleSubmit}
      >
        Simpan Data
      </button>
    </div>
  );
}

export default Form;