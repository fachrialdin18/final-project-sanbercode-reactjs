import React from "react";

function Product({ data }) {
  return (
      <div className="bg-white shadow-xl rounded p-4 mx-3">
        <img src={data.image_url} alt="Image 1" className="w-full h-64 object-cover" />
        <h1 className="text-gray-700 text-xl font-bold mt-2">{data.name}</h1>
        <p className="text-blue-700 text-sm font-bold mt-2">{data.category}</p>
        <h2 className="text-blue-400 text-md font-semibold mt-5">{data.harga_diskon_display}</h2>
        <p className="text-red-500 text-sm line-through font-semibold mt-1">{data.harga_display}</p>
        <p className="text-gray-500 text-sm font-bold mt-2">Stock : {data.stock}</p>
      </div>
  );
}

export default Product;
