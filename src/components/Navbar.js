import { Button } from "antd";
import axios from "axios";
import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import Logo from '../images/logo.png'

function Navbar() {
  const [isOpen, setIsOpen] = useState(false);
  const location = useLocation();
  const navigate = useNavigate();
  const onLogout = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/logout",
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      alert("Berhasil Keluar Akun !");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    } finally {
      // Penghapusan localstorage
      localStorage.removeItem("token");
      localStorage.removeItem("username");
      navigate("/login");
    }
  };
  return (
    <nav className="flex items-center justify-between flex-wrap p-6 bg-gray-800 text-white">
       <div className="flex items-center flex-shrink-0 text-white mr-6 lg:mr-72">
          <img src={Logo} className="w-200 h-10 mr-2" alt="Logo" />
       </div>

       <div className="block lg:hidden">
          <button
            onClick={() => setIsOpen(!isOpen)}
            className="flex items-center px-3 py-2 rounded text-black-500 hover:text-black-400"
          >
            <svg
              className={`fill-current h-3 w-3 ${isOpen ? "hidden" : "block"}`}
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
            </svg>
            <svg
              className={`fill-current h-3 w-3 ${isOpen ? "block" : "hidden"}`}
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M10 8.586L2.929 1.515 1.515 2.929 8.586 10l-7.071 7.071 1.414 1.414L10 11.414l7.071 7.071 1.414-1.414L11.414 10l7.071-7.071-1.414-1.414L10 8.586z" />
            </svg>
          </button>
       </div>

       <div
        className={`w-full block flex-grow lg:flex lg:items-center lg:w-auto ${isOpen ? "block" : "hidden"}`}
       >
          <div className="text-sm lg:flex-grow">
            <Link className={`${location.pathname === '/' ? 'text-blue-400 font-bold shadow-lg shadow-blue-900/40' : ''}`} to="/">
              <a className="block mt-4 lg:inline-block lg:mt-0 text-white-100 text-lg mb-2 mr-4 ml-1">
                Home
              </a>
            </Link>
            <Link className={`${location.pathname === '/table' ? 'text-blue-400 font-bold shadow-lg shadow-blue-900/40' : ''}`} to="/table">
              <a className="block mt-4 lg:inline-block lg:mt-0 text-white-100 text-lg mb-2 mr-4 ml-1">
                Table
              </a>
            </Link>
            <Link className={`${location.pathname === '/form' ? 'text-blue-400 font-bold shadow-lg shadow-blue-900/40' : ''}`} to="/form">
              <a className="block mt-4 lg:inline-block lg:mt-0 text-white-100 text-lg mb-2 mr-4 ml-1">
                Form
              </a>
            </Link>
          </div>

          <div className>
          {localStorage.getItem("token") != null ? (
                <>
                  <button className="inline-flex items-center rounded-lg bg-white-400 border-0 py-2 px-1 text-white mr-3">
                      <h1>Halo, Welcome <b className="text-blue-400 shadow-lg shadow-blue-900/40">{localStorage.getItem("username")}</b> !</h1>
                  </button>
                  <button onClick={onLogout} className="text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 shadow-lg shadow-red-500/50 dark:shadow-lg dark:shadow-red-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">
                      Sign Out
                  </button>
                </>
              ) : (
                <>
                  <Link className="no-underline" to="/login">
                    <button className="text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">
                        Login
                    </button>
                  </Link>
                  <Link className="no-underline" to="/register">
                    <button className="text-gray-900 bg-gradient-to-r from-lime-200 via-lime-400 to-lime-500 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-lime-300 dark:focus:ring-lime-800 shadow-lg shadow-lime-500/50 dark:shadow-lg dark:shadow-lime-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">
                        Register
                    </button>
                  </Link>
                </>
              )}
          </div>
       </div>
    </nav>
  );
}

export default Navbar;