import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { ProductContext } from "../context/ProductContext";

function TableProduct() {
  const { products, fetchProducts, moveToCreate } = useContext(ProductContext);

  const [filter, setFilter] = useState({
    search: "",
    category: "",
  });

  const [filterProducts, setFilterProducts] = useState([]);
  
  const handleSearch = () => {
    console.log(filter);
    //kita manipulasi data dari filterarticles
    let tmpProducts = structuredClone(products); //ngecopy data dari products context
    if (filter.search !== ""){
      //jalankan search
      tmpProducts = tmpProducts.filter((item) => {
        return item.name.toLowerCase().includes(filter.search.toLowerCase());
      });
    }

    if (filter.category !== ""){
      //jalankan search
      tmpProducts = tmpProducts.filter((item) => {
        return item.category.toString() === filter.category;
      });
    }
    if (filter.category !== ""){
  
    }
    setFilterProducts(tmpProducts);
  };

  const handleReset = () => {
    setFilter({
      search : "",
      category : "",
    });
    setFilterProducts(products);
  };

  const onUpdate = (article) => {
    // alert(id);
  };

  const handleChangeInput = (event) => {
    if (event.target.name === "category"){
      setFilter({
        ...filter,
        category: event.target.value,
      });
    } else if (event.target.name === "search"){
      setFilter({
        ...filter,
        search: event.target.value,
      });
    }
  };
  
  const onDelete = async (id) => {
    // alert(`Berhasil Menghapus Artikel dengan ID: ${id}`);
    try {
      // fetch data
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/final/products/${id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      alert("Berhasil Menghapus Product");
      fetchProducts();
    } catch (error) {
      console.log(error);
      alert("Gagal Menghapus Product");
    }
  };

  useEffect(() =>{
    setFilterProducts(products);
  }, [products]);


  return (
    <section>
      <h1 className="my-8 text-3xl font-semibold text-center text-gray-700">Table Article</h1>
      <div className="max-w-4xl mx-auto w-full my-4 px-2 py-2">
        <div className="flex gap-3 my-3">
          <select value={filter.category} onChange={handleChangeInput} name="category" className="bg-gray-50 rounded-lg py-3 px-3">
            <option value={null} className="bg-gray-800 text-blue-400">Silahkan Pilih Kategori</option>
              <option value="teknologi">Teknologi</option>
              <option value="makanan">Makanan</option>
              <option value="minuman">Minuman</option>
              <option value="hiburan">Hiburan</option>
              <option value="kendaraan">Kendaraan</option>
          </select>
        </div>
        <input onChange={handleChangeInput} className="block p-2.5 w-96 z-20 text-sm text-gray-900 bg-white-100 rounded-lg focus:ring-blue-500 focus:border-blue-500" name="search" value={filter.search} type="search" placeholder="Cari Product" />
        <div className="my-3 ">
          <button onClick={handleSearch} className="text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">
              Cari
          </button>
          <button onClick={handleReset} className="text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">
              Hapus
          </button>
        </div>
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
          <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
              <thead class="text-xs text-gray-700 uppercase bg-white-100 ">
                  <tr>
                      <th scope="col" class="px-6 py-3">
                          <span class="sr-only">Image</span>
                      </th>
                      <th scope="col" class="px-6 py-3">
                          Pembuat
                      </th>
                      <th scope="col" class="px-6 py-3">
                          Product
                      </th>
                      <th scope="col" class="px-6 py-3">
                          Action
                      </th>
                  </tr>
              </thead>
              <tbody className="text-gray-700">
                {filterProducts.map((item, index) => {
                  if (localStorage.getItem("username") === item.user.username ) {
                    return (
                        <tr class="bg-white border-b text-gray-700 hover:bg-gray-50 dark:hover:bg-white-100">
                          <td class="w-32 p-4">
                              <img src={item.image_url} alt="Apple Watch" />
                          </td>
                          <td class="px-6 py-4 font-semibold text-gray-900">
                              {item.user.username}
                          </td>
                          <td class="px-6 py-4 font-semibold text-gray-900">
                              {item.name}
                          </td>
                          <td class="px-6 py-4">
                              <Link to={`/update/${item.id}`}>
                                 <a class="font-medium text-blue-600 hover:underline ml-2">Perbarui</a>
                              </Link>
                              <a onClick={() => onDelete(item.id)} class="font-medium text-red-600 hover:underline ml-2">Hapus</a>
                          </td>                 
                        </tr>
                  );
                  }
                })}  
              </tbody>
          </table>
        </div>
      </div>
    </section>
  );
}

export default TableProduct;