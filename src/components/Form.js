import { Button, Checkbox, Form, Input } from "antd";
import axios from "axios";
import { useFormik } from "formik";
import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { ProductContext } from "../context/ProductContext";

const rulesSchema = Yup.object({
  name: Yup.string().required("Nama Artikel wajib diisi"),
  content: Yup.string().required("Konten Artikel wajib diisi"),
  highlight: Yup.bool().required("Highlight wajib diisi"),
  image_url: Yup.string()
    .required("Password wajib diisi")
    .url("Link Gambar tidak valid"),
});

function FormCreate() {
  const { fetchProducts } = useContext(ProductContext);
  const initialState = {
    name: "",
    content: "",
    image_url: "",
    highlight: false,
  };
  const navigate = useNavigate();

  const storeArticle = async (values) => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/final/products",
        {
          name: values.name,
          image_url: values.image_url,
          highlight: values.highlight,
          content: values.content,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      alert("Berhasil Mengirim Request");
      // memannggil data kembali
      fetchProducts();
      resetForm();
      // navigasi ke halaman table
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
    setFieldValue,
    setFieldTouched,
  } = useFormik({
    initialValues: initialState,
    onSubmit: storeArticle,
    validationSchema: rulesSchema,
  });

  return (
    <div>
      <section className="max-w-xl mx-auto border-2 border-solid border-gray-600 p-6 mt-12">
        <h1 className="text-center text-2xl">Form Create Products</h1>
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          className="max-w-2xl"
        >
          <Form.Item
            label="Nama"
            name="name"
            help={touched.name === true && errors.name}
            hasFeedback={true}
            validateStatus={errors.name === true && "errors"}
          >
            <Input
              type="text"
              placeholder="Masukkan nama"
              onChange={handleChange}
              onBlur={handleBlur}
              name="name"
              value={values.name}
            />
          </Form.Item>
          <Form.Item
            label="Konten"
            name="content"
            help={touched.content === true && errors.content}
            hasFeedback={true}
            validateStatus={errors.content === true && "errors"}
          >
            <Input
              type="text"
              placeholder="Masukkan nama"
              onChange={handleChange}
              onBlur={handleBlur}
              name="content"
              value={values.content}
            />
          </Form.Item>
          <Form.Item
            label="Image URL"
            name="image_url"
            help={touched.image_url === true && errors.image_url}
            hasFeedback={true}
            validateStatus={errors.image_url === true && "errors"}
          >
            <Input
              type="text"
              placeholder="Masukkan nama"
              onChange={handleChange}
              onBlur={handleBlur}
              name="image_url"
              value={values.image_url}
            />
          </Form.Item>
          <Form.Item
            name="highlight"
            help={touched.highlight === true && errors.highlight}
            hasFeedback={true}
            validateStatus={errors.highlight === true && "errors"}
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Checkbox
              onChange={(e) => setFieldValue("highlight", e.target.checked)}
              onBlur={() => setFieldTouched("highlight")}
              name="highlight"
              checked={values.highlight}
            >
              Is Highlight
            </Checkbox>
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" onClick={handleSubmit}>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </section>
    </div>
  );
}

export default FormCreate;
