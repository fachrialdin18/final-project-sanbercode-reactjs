import axios from "axios";
import { createContext, useState } from "react";
import { useNavigate } from "react-router-dom";

export const ProductContext = createContext();

export const ProductProvider = ({ children }) => {
  const [products, setProduct] = useState([]);
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState("success");
  const navigate = useNavigate();

  const fetchProducts = async () => {
    try {
      // fetch data
      setLoading(true); // penanda kalau kita mau mengambil data
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/final/products",
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      console.log(response.data.data);
      setProduct(response.data.data);
      setStatus("success");
    } catch (error) {
      console.log(error);
      setStatus("error");
    } finally {
      // bakal selalu kejalan mau codingan errro atau ngga
      setLoading(false);
    }
  };

  const moveToCreate = () => {
    navigate("/create");
  };

  return (
    <ProductContext.Provider
      value={{
        products,
        setProduct,
        loading,
        setLoading,
        status,
        setStatus,
        fetchProducts,
        moveToCreate,
      }}
    >
      {children}
    </ProductContext.Provider>
  );
};
